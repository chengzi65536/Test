//
//  AppDelegate.h
//  Test
//
//  Created by 橙子 on 18/5/28.
//  Copyright © 2018年 Orange. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

