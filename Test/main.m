//
//  main.m
//  Test
//
//  Created by 橙子 on 18/5/28.
//  Copyright © 2018年 Orange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
