//
//  OilCarBuyController.m
//  FreightDriver
//
//  Created by 橙子 on 18/5/31.
//  Copyright © 2018年 chen. All rights reserved.
//

#import "OilCarBuyController.h"
#import "OilCarCell.h"
#import "UctHelper.h"
#import "OilCarBuyRecordController.h"

@interface OilCarBuyController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) NSArray *titleArr;
@property (nonatomic, strong) NSMutableArray *descArr;
@property (nonatomic, strong) UctHelper *helper;

@end

@implementation OilCarBuyController

- (void)viewDidLoad {
    [super viewDidLoad];
    //默认显示
    _descArr = [NSMutableArray arrayWithObjects:@"选择油卡",@"收费账户",@"油卡面值",@"油卡收费",nil];
    
    [self setUILayout];
}

- (void)setUILayout{
    UIButton *btn = [UIButton jc_textButton:@"购买记录" fontSize:16 normalColor:[UIColor whiteColor] highlightedColor:[UIColor whiteColor]];
    [btn addTarget: self action:@selector(buyRecordAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn sizeToFit];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = rightItem;
}
- (void)buyRecordAction:(UIButton *)btn {
    
    OilCarBuyRecordController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"OilCarBuyRecordController"];
    controller.clevel = SecondViewController;
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)comfireBtnAction:(UIButton *)sender {
}
- (IBAction)cancelBtnAction:(UIButton *)sender {
}

#pragma mark - UITableView代理

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.titleArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OilCarCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OilCarCell" forIndexPath:indexPath];
    if (indexPath.row == 0 || indexPath.row == 2) {
        [cell setTitle:self.titleArr[indexPath.row] desc:self.descArr[indexPath.row] showIcon:YES showLine:YES];
    } else {
        [cell setTitle:self.titleArr[indexPath.row] desc:self.descArr[indexPath.row] showIcon:NO showLine:YES];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
         //网络请求获取油卡
        [self.helper getOilCarListWithComplete:^(NSArray *daraArr, NSString *code) {
            
        }];
    }
}

- (void)showSheetWith:(NSArray *)titleArr Title:(NSString *)title IndexPath:(NSIndexPath *)indexPath Complete:(void(^)(NSInteger,NSString *))complete {
    
    //显示弹出框列表选择
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    
    for (int i = 0; i<titleArr.count; i++) {
        
        UIAlertAction* action = [UIAlertAction actionWithTitle:titleArr[i] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            
            if (complete) {
                complete(i,action.title);
            }
        }];
        [alert addAction:action];
    }
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}


#pragma mark - 懒加载

- (UctHelper *)helper {
    if (!_helper) {
        _helper = [[UctHelper alloc]init];
    }
    return _helper;
}
- (NSArray *)titleArr {
    if (!_titleArr) {
        _titleArr = @[@"选择油卡",@"收费账户",@"油卡面值",@"油卡收费"];
    }
    return _titleArr;
}

@end
